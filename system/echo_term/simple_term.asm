.mode NOP_DELAY

.address 0x2000

	movi	r40,	!string
	nop

!prompt
	load	r20,	[r40]
	brae	{F}	r20,	r0,	[!done]

	stor	r20,	[0x4000]
	add	r40,	r40,	(1)
	bra	[!prompt]
!done
	movi r41, 0xD
	movi r42, 0xA
	stor r41, [0x4000]
	stor r42, [0x4000]

!main
	LOAD r1, [0x4002] # Read from the KB
	STOR r1, [0x4000] # Write to the terminal
	BRAE {F} r0, r0, [!main]

!string
    "Miniat Echo Terminal, type a string hit enter and the miniat echo's it back!"
