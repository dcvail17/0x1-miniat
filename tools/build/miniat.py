#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import SCons.Builder


class MiniatBuilder(object):

    def __init__(
        self,
        rootdir,
        libdir,
        exedir,
        bindir,
        docdir,
        env,
        variant_dir,
        ):

	self._mash = 'mash'
        self._lemon = 'lemon'
        self._rootdir = rootdir
        self._libdir = libdir
        self._exedir = exedir
        self._bindir = bindir
        self._docdir = docdir
        self._variant_dir = variant_dir
        self._env = env
        self._env.Append(LIBPATH=libdir)
        self._env.Append(CPPPATH='#/vm/inc')
        self._env.Append(CFLAGS='-g')
        self._env.PrependENVPath('PATH', exedir)
        self._prog_suffix = self._env['PROGSUFFIX']
        self._lib_suffix = self._env['LIBSUFFIX']

        bld = SCons.Builder.Builder(action='flex -i -o $TARGET $SOURCE'
                                    , src_suffix='.flex')
        self._env.Append(BUILDERS={'Flex': bld})

        bld = SCons.Builder.Builder(action=self._lemon
                                    + self._prog_suffix + ' $SOURCE',
                                    emitter=self._lemon_emitter)
        self._env.Append(BUILDERS={'Lemon': bld})

        bld = SCons.Builder.Builder(action=self._mash
                                    + self._prog_suffix + ' $SOURCE',
                                    src_suffix='.asm', suffix='.bin')
        self._env.Append(BUILDERS={'Mash': bld})

    def _lemon_emitter(
        self,
        target,
        source,
        env,
        ):

        for s in source:
            (dirName, fileName) = os.path.split(str(s))
            (fileBaseName, fileExtension) = os.path.splitext(fileName)
            outHeaderName = os.path.join(dirName, fileBaseName + '.h')
            outSrcName = os.path.join(dirName, fileBaseName + '.c')
            target.append(outHeaderName)
            target.append(outSrcName)

	for t in target:
		s = str(t)
		ext = os.path.splitext(s)[1]
		if len(ext) == 0:
			target.remove(t)

        return (target, source)

    def getenv(self):
        return self._env

    def buildMash(
        self,
	INCLUDES=[],
	SRC_FILES=[],
	LIBS=[],
	):
        p = self._env.Program(self._mash, SRC_FILES, CPPPATH=INCLUDES,
                              LIBS=LIBS, LIBPATH=self._libdir)
        x = self._env.Install(self._exedir, p)
	return [p, x]

    def executable(
        self,
        NAME=None,
        INCLUDES=[],
        SRC_FILES=[],
        LIBS=[],
        ):

        p = self._env.Program(NAME, SRC_FILES, CPPPATH=INCLUDES,
                              LIBS=LIBS, LIBPATH=self._libdir)
        x = self._env.Install(self._exedir, p)
        return [p, x]

    def sharedlib(
        self,
        NAME=None,
        INCLUDES=[],
        SRC_FILES=[],
        LIBS=[],
        ):

        l = self._env.SharedLibrary(NAME, SRC_FILES, CPPPATH=INCLUDES,
                                    LIBS=LIBS)
        x = self._env.Install(self._libdir, l)
        return [l, x]

    def staticlib(
        self,
        NAME=None,
        INCLUDES=[],
        SRC_FILES=[],
        LIBS=[],
        ):

        l = self._env.StaticLibrary(NAME + self._lib_suffix, SRC_FILES,
                                    CPPPATH=INCLUDES, LIBS=LIBS)
        x = self._env.Install(self._libdir, l)
        return [l, x]

    def flex(self, NAME, SRC):

        return self._env.Flex(NAME, SRC)

    def lemon(self, SRC):

        l = self._env.Lemon(SRC)
        d = self._env.Depends(l, os.path.join(self._exedir, self._lemon
                              + self._prog_suffix))
        return [l, d]


    def mash(self, SRC):

        m = self._env.Mash(SRC)
        self._env.Depends(m, os.path.join(self._exedir, self._mash
                          + self._prog_suffix))
        return self._env.Install(self._bindir, m)

    def prebuilttool(
        self,
        name,
        path,
        layout='',
        prog='',
        ):

        install_path = os.path.join(self._exedir, name)
        bld = SCons.Builder.Builder(action=prog + ' ' + '"'
                                    + install_path + '"' + ' ' + layout)
        self._env.Append(BUILDERS={name: bld})

        return self._env.InstallAs(install_path, path)

    def tool(
        self,
        name,
        target,
        src,
        ):

        return getattr(self._env, name)(target, src)

    def doc(self, name, path):
        return self._env.InstallAs(os.path.join(self._docdir, name),
                                   path)


