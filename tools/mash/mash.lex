%option noyywrap
%{
/*
 * Name        : mash.lex
 * Author      : William "Amos" Confer
 * 
 * License     : Copyright 2008, 2009, 2012 William "Amos" Confer
 *
 *               This file is part of MiniAT.
 *
 *               MiniAT is free software: you can redistribute it and/or modify
 *               it under the terms of the GNU General Public License as published by
 *               the Free Software Foundation, either version 3 of the License, or
 *               (at your option) any later version.
 *
 *               MiniAT is distributed in the hope that it will be useful,
 *               but WITHOUT ANY WARRANTY; without even the implied warranty of
 *               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *               GNU General Public License for more details.
 *
 *               You should have received a copy of the GNU General Public License
 *               along with MiniAT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "miniat/miniat.h"

#include "token.h"
#include "mash.h"

//#define FLEX_TOKEN_PRINT

extern void *ParseAlloc(void *(*mallocProc)(size_t));

void *parser;
Token T;
char *tmp_str;

/* Requires two passes... (1) to build symbol, address, and value tables for
 * constants and labels, and (2) to generate output binary file if stage (1) 
 * passes without error.
 */
int assembler_pass; 
int line_num;
int char_pos;

char * filename;

m_sword ascii_to_value(char *s, char base) {
	const char table[] = "0123456789abcdef";
	m_sword result = 0;
	while(*s != '\0') {
		if(*s != '_') {
			result *= base;
			result += (strchr(table, tolower(*s)) - table) / sizeof(char);
		}
		s++;
	}
	return result;
}

char *get_yystring() {
	char * s = malloc(sizeof(char) * (1 << 16));
	memcpy(s, yytext, yyleng);
	s[yyleng] = '\0';
	s = realloc(s, sizeof(char) * (strlen(s) + 1));
	return s;
}

char *pack_token(Token *t) {
	t->token_str = get_yystring();
	t->is_rci_exp = 0; /* these expressions are determined at parse */
	t->line_num = line_num;
	t->char_pos = char_pos;
	char_pos += yyleng;
#ifdef FLEX_TOKEN_PRINT
	printf("%s\n", t->token_str);
#endif
	return t->token_str;
}

%}

HEX_DIGIT [a-f0-9]
DEC_DIGIT [0-9]
OCT_DIGIT [0-7]
BIN_DIGIT [01]

ALPHA    [a-z]
ALPHANUM [a-z0-9]

%%

parse_trace_on	{ ParseTrace(stdout, "TRACE:\t"); }
parse_trace_off { ParseTrace(NULL, NULL); }

add	{ pack_token(&T); Parse(parser, TOKEN_ADD, T); }
sub	{ pack_token(&T); Parse(parser, TOKEN_SUB, T); }
mult	{ pack_token(&T); Parse(parser, TOKEN_MULT, T); }
div	{ pack_token(&T); Parse(parser, TOKEN_DIV, T); }
mod	{ pack_token(&T); Parse(parser, TOKEN_MOD, T); }

and	{ pack_token(&T); Parse(parser, TOKEN_AND, T); }
or	{ pack_token(&T); Parse(parser, TOKEN_OR, T); }
exor	{ pack_token(&T); Parse(parser, TOKEN_EXOR, T); }
shl	{ pack_token(&T); Parse(parser, TOKEN_SHL, T); }
shr	{ pack_token(&T); Parse(parser, TOKEN_SHR, T); }

load	{ pack_token(&T); Parse(parser, TOKEN_LOAD, T); }
stor	{ pack_token(&T); Parse(parser, TOKEN_STOR, T); }
rload	{ pack_token(&T); Parse(parser, TOKEN_RLOAD, T); }
rstor	{ pack_token(&T); Parse(parser, TOKEN_RSTOR, T); }

brae	{ pack_token(&T); Parse(parser, TOKEN_BRAE, T); }
brane	{ pack_token(&T); Parse(parser, TOKEN_BRANE, T); }
bral	{ pack_token(&T); Parse(parser, TOKEN_BRAL, T); }
brale	{ pack_token(&T); Parse(parser, TOKEN_BRALE, T); }
brag	{ pack_token(&T); Parse(parser, TOKEN_BRAG, T); }
brage	{ pack_token(&T); Parse(parser, TOKEN_BRAGE, T); }
int	{ pack_token(&T); Parse(parser, TOKEN_INT, T); }
iret	{ pack_token(&T); Parse(parser, TOKEN_IRET, T); }

neg	{ pack_token(&T); Parse(parser, TOKEN_NEG, T); }
inv	{ pack_token(&T); Parse(parser, TOKEN_INV, T); }
movr	{ pack_token(&T); Parse(parser, TOKEN_MOVR, T); }
movi	{ pack_token(&T); Parse(parser, TOKEN_MOVI, T); }
bra	{ pack_token(&T); Parse(parser, TOKEN_BRA, T); }
nop	{ pack_token(&T); Parse(parser, TOKEN_NOP, T); }
flush	{ pack_token(&T); Parse(parser, TOKEN_FLUSH, T); }


nop_delay	{
	/* mode */
	pack_token(&T);
	Parse(parser, TOKEN_NOP_DELAY, T);
}

no_nop_delay	{
	/* mode */
	pack_token(&T);
	Parse(parser, TOKEN_NO_NOP_DELAY, T);
}

simple_vars	{
	/* mode */
	pack_token(&T);
	Parse(parser, TOKEN_SIMPLE_VARS, T);
}

no_simple_vars	{
	pack_token(&T);
	Parse(parser, TOKEN_NO_SIMPLE_VARS, T);
}

\"[^"\n]*\"	{
	/* double-quoted string */
	pack_token(&T);
	T.token_str++;
	T.token_str[strlen(T.token_str) - 1] = '\0';
	Parse(parser, TOKEN_STRING, T);
	
}

"__"	{
	/* predication prefix */
	pack_token(&T);
	Parse(parser, TOKEN_PREDICATE, T);
}


\{[TF]\}	{
	/* boolean for hinting */
	pack_token(&T);
	T.data = T.token_str[1];
	Parse(parser, TOKEN_HINT, T);
}



\.uses[ \t]	{
	/* perform an embeded-link to another assembly file */
	pack_token(&T);
	Parse(parser, TOKEN_USES_DIRECTIVE, T);
}


\.mode[ \t]	{
	/* set a particular mode */
	pack_token(&T);
	Parse(parser, TOKEN_MODE_DIRECTIVE, T);
}


\.(address|addr)[ \t]	{
	/* address to write to */
	pack_token(&T);
	Parse(parser, TOKEN_ADDR_DIRECTIVE, T);
}

\.(constant|const)[ \t]	{
	/* constant definition */
	pack_token(&T);
	Parse(parser, TOKEN_CONST_DIRECTIVE, T);
}

\.(variable|var)[ \t]	{
	/* variable declaration */
	pack_token(&T);
	Parse(parser, TOKEN_VAR_DIRECTIVE, T);
}

0x(_*{HEX_DIGIT})+	{
	/* unsigned hex number */
	tmp_str = pack_token(&T);
	tmp_str += 2;
	T.data = ascii_to_value(tmp_str, 16);
	Parse(parser, TOKEN_NUMBER, T);
}

0d(_*{DEC_DIGIT})+	{
	/* signed decimal number */
	tmp_str = pack_token(&T);
	/* skip optional "0d" prefix */
	tmp_str += 2;
	T.data = ascii_to_value(tmp_str, 10);
	Parse(parser, TOKEN_NUMBER, T);
}

{DEC_DIGIT}+(_*{DEC_DIGIT})*	{
	/* signed decimal number */
	tmp_str = pack_token(&T);
	T.data = ascii_to_value(tmp_str, 10);
	Parse(parser, TOKEN_NUMBER, T);
}


0c(_*{OCT_DIGIT})+	{
	/* unsigned octal number */
	tmp_str = pack_token(&T);
	tmp_str += 2;
	T.data = ascii_to_value(tmp_str, 8);
	Parse(parser, TOKEN_NUMBER, T);
}


0b(_*{BIN_DIGIT})+	{
	/* unsigned hex number */
	tmp_str = pack_token(&T);
	tmp_str += 2;
	T.data = ascii_to_value(tmp_str, 2);
	Parse(parser, TOKEN_NUMBER, T);
}

r{DEC_DIGIT}+	{
	/* general purpose register */
	pack_token(&T);
	T.data = ascii_to_value(T.token_str + 1, 10);
	Parse(parser, TOKEN_REGISTER, T);
}

rsp	{
	/* r253: register stack pointer */
	pack_token(&T);
	T.data = 253;
	Parse(parser, TOKEN_REGISTER, T);
}

sp	{
	/* r254: register stack pointer */
	pack_token(&T);
	T.data = 254;
	Parse(parser, TOKEN_REGISTER, T);
}

pc	{
	/* r255: register stack pointer */
	pack_token(&T);
	T.data = 255;
	Parse(parser, TOKEN_REGISTER, T);
}


{ALPHA}(_?{ALPHANUM})*	{
	/* identifier */
	pack_token(&T);
	Parse(parser, TOKEN_IDENTIFIER, T);
}

\'.\'	{
	/* character immediate */
	pack_token(&T);
	T.data = yytext[1];
	Parse(parser, TOKEN_CHARACTER, T);
}

\'[^'\n]*\'	{
	/* single-quoted "packed" string */
	pack_token(&T);
	T.token_str++;
	T.token_str[strlen(T.token_str) - 1] = '\0';
	Parse(parser, TOKEN_PACKED_STRING, T);
	
}

"["	{
	/* left bracket */
	pack_token(&T);
	Parse(parser, TOKEN_LBRACKET, T);
}

"]"	{
	/* right bracket */
	pack_token(&T);
	Parse(parser, TOKEN_RBRACKET, T);
}

"("	{
	/* left paren */
	pack_token(&T);
	Parse(parser, TOKEN_LPAREN, T);
}

")"	{
	/* right paren */
	pack_token(&T);
	Parse(parser, TOKEN_RPAREN, T);
}



[+\-]	{
	/* arithmetic operators */
	pack_token(&T);
	Parse(parser, TOKEN_ARITHMETIC_OPERATOR, T);
}

"!"	{
	/* label */
	pack_token(&T);
	Parse(parser, TOKEN_LABEL_DIRECTIVE, T);
}




\n	{
	/* EOL */
	pack_token(&T);
	Parse(parser, TOKEN_EOL, T);
	line_num++;
	char_pos = 1;

}



[ \t]+	{
	/* whitespace */
	pack_token(&T);
}

"#".*	{
	/* line comment */
	pack_token(&T);
}

"<<<"((">"|">>")?[^>]+)*">>>"	{
	/* block comments between <<< and >>> */
	pack_token(&T);
	
	/* count the number of newlines */
	int newline_cnt = 0;
	char *str = T.token_str;
	while(str = strchr(str, '\n')) {
		newline_cnt++;
		while(str[0] == '\n' || str[0] == '\r') {
			str++;
		}
	}
	
	line_num += newline_cnt;
	if(newline_cnt > 0) {
		char_pos = 1;
		str = strrchr(T.token_str, '\n');
		while(str[0] == '\n' || str[0] == '\r') {
			str++;
		}
		char_pos += strlen(str);		
	}
	
	
}




","	{
	/* comma */
	pack_token(&T);
	Parse(parser, TOKEN_COMMA, T);
}
	

.	{
	/* ERROR!!!! */
	pack_token(&T);
	if(assembler_pass == 1) {
		fprintf(stderr, "Error, line %d:%d - unexpected character\"%s\"\n", T.line_num, T.char_pos, get_yystring());
	}
}

%%

int main(int argc, char **argv) {
	extern int errors_found;
	extern m_uword address;
	extern char *constants[];
	extern char *labels[];
	extern unsigned char constant_values[];
	extern unsigned char label_addrs[];
	extern int constant_cnt;
	extern int label_cnt;
	
	int i;

	argv++; argc--;

	if(argc == 1) {
		if(strcmp(argv[0] + strlen(argv[0]) - 4, ".asm") != 0) {
			fprintf(stderr, "Error - \".asm\" file expected\n");
			exit(1);
		}

		filename = (char*)malloc(sizeof(char) * (strlen(argv[0]) + 1));
		if(filename == NULL) {
			fprintf(stderr, "Error - could not allocate memory sufficient for this application\n");
			exit(1);
		}
		/* strcpy(filename + strlen(filename) - 3, "bin"); */
		strcpy(filename, argv[0]);		
		strcpy(strrchr(filename, '.'), ".bin");

		yyin = fopen(argv [0], "r");
		if(yyin != NULL) {
			parser = ParseAlloc(malloc);

			/* first pass of the assembler. see comment at the
			* top of file for details.
			*/
			assembler_pass = 1;
			line_num = 1;
			char_pos = 1;
			yylex();
			Parse(parser, 0, 0);
			
			if(errors_found == 0) {
				/* second pass of the assembler. see comment at the
				* top of file for detatils.
				*/
				assembler_pass = 2;
				line_num = 1;
				char_pos = 1;
				address = 0;
				rewind(yyin);
				yylex();
				Parse(parser, 0, 0);
			}
			ParseFree(parser, free);
			fclose(yyin);
		}
		free(filename);		
	}
	else {
		fprintf(stderr, "no input file given\n");
	}
	return errors_found;
}
