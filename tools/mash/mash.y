%include {
	/*
	 * Name        : mash.y
	 * Author      : William "Amos" Confer
	 * 
	 * License     : Copyright 2008, 2009, 2012 William "Amos" Confer
	 *
	 *               This file is part of MiniAT.
	 *
	 *               MiniAT is free software: you can redistribute it and/or modify
	 *               it under the terms of the GNU General Public License as published by
	 *               the Free Software Foundation, either version 3 of the License, or
	 *               (at your option) any later version.
	 *
	 *               MiniAT is distributed in the hope that it will be useful,
	 *               but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *               GNU General Public License for more details.
	 *
	 *               You should have received a copy of the GNU General Public License
	 *               along with MiniAT.  If not, see <http://www.gnu.org/licenses/>.
	 */

	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <assert.h>
	
	#include "miniat_priv_defines.h"
	
	#include "token.h"
	
	#define MAX_CNT 1024

	typedef struct block block;
	typedef struct block_item block_item;	

	struct block {
		int node_cnt;
		block_item *head;
		block_item *tail;
	};

	struct block_item {
		m_uword words[2];
		bool is_instruction;
		m_uword address;
		block_item *next;
	};

	extern int assembler_pass;
	extern char* filename;

	m_uword address = 0;

	int block_cnt = 0;
	block blocks[MAX_CNT];
	
	char *labels[MAX_CNT];
	m_uword label_addrs[MAX_CNT];
	int label_cnt = 0;
	
	char *constants[MAX_CNT];
	m_uword constant_values[MAX_CNT];
	int constant_cnt = 0;

	char *variables[MAX_CNT];
	m_uword variable_addrs[MAX_CNT];
	int variable_cnt = 0;
	int variable_top = 0x1A00;

	int errors_found = 0;
	int line_cnt = 0;

	int mode_nop_delay = 1;
	const int DELAY_NOP_CYCLES = 3;
	int mode_simple_vars = 1;

	void message (int err, Token t, const char *primary_err, const char *optional_err) {
		if (err) {
			fprintf (stderr, "Error, ");
		}
		else {
			fprintf (stderr, "Warning, ");
		}
		fprintf (stderr, "line %d:%d - %s", t.line_num, t.char_pos, primary_err);
		if (optional_err != NULL) {
			fprintf (stderr, ": \"%s\"", optional_err);
		}
		fprintf (stderr, "\n");
	}
	
	void error (Token t, const char *primary_err, const char *optional_err) {
		errors_found++;
		message (1, t, primary_err, optional_err);
	}
	
	void warning (Token t, const char *primary_warning, const char *optional_warning) {
		message (0, t, primary_warning, optional_warning);
	}


	int variable_pos(const char* s) {
		int result = variable_cnt - 1;
		while(result >= 0) {
			if(strcmp(variables[result], s) == 0) break;
			result--;
		}
		return result;
	}

	
	int constant_pos(const char* s) {
		int result = constant_cnt - 1;
		while(result >= 0) {
			if(strcmp(constants[result], s) == 0) break;
			result--;
		}
		return result;
	}
	
	int label_pos(const char* s) {
		int result = label_cnt - 1;
		while(result >= 0) {
			if(strcmp(labels[result], s) == 0) break;
			result--;
		}
		return result;
	}
	
	void insert_instruction(m_uword pred, m_uword inst, m_uword hint, m_uword reg_a, m_uword reg_b, m_uword reg_c, m_uword immed) {
		block *b = &(blocks[block_cnt]);

		block_item *item = malloc(sizeof(block_item));
		item->address = address;
		item->is_instruction = true;

		item->words[0] = 0;
		item->words[0] |= hint << M_HINT_START;
		item->words[0] |= pred << M_PREDICATION_START;
		item->words[0] |= inst << M_OPCODE_START;
		item->words[0] |= reg_a << M_REGISTER_A_START;
		item->words[0] |= reg_b << M_REGISTER_B_START;
		item->words[0] |= reg_c << M_REGISTER_C_START;

		item->words[1] = immed;

		item->next = NULL;
	
		if(b->node_cnt != 0) {
			/* add to the end of the list */
			b->tail->next = item;
			b->tail = item;
		}
		else {
			/* it's the first item in this block */
			b->head = b->tail = item;
		}
		
		blocks[block_cnt].node_cnt++;
		address += 2;
	}

	void insert_raw(m_uword data) {
		block *b = &(blocks[block_cnt]);

		block_item *item = malloc(sizeof(block_item));
		item->address = address;
		item->is_instruction = false;

		item->words[0] = data;

		item->next = NULL;
	
		if(b->node_cnt != 0) {
			/* add to the end of the list */
			b->tail->next = item;
			b->tail = item;
		}
		else {
			/* it's the first item in this block */
			b->head = b->tail = item;
		}

		blocks[block_cnt].node_cnt++;
		address += 1;
	}
	
	void nop_delay_slots() {
		if(mode_nop_delay) {
			int i;
			for(i = 0; i < DELAY_NOP_CYCLES; i++) {
				insert_instruction(0, 0, 0, 0, 0, 0, 0);
			}
		}
	}
}

%start_symbol program

%token_type {Token}
%token_prefix {TOKEN_}

%syntax_error {
	fprintf (stderr, "Syntax error, line %d:%d - \"%s\"\n", TOKEN.line_num
	, TOKEN.char_pos, TOKEN.token_str);
	errors_found++;
}

%parse_failure {
	fprintf (stderr, "Giving up... the parser has gotten completely lost :-x\n");
}

%stack_overflow {
	fprintf (stderr, "Giving up... parser stack overflow :-x\n");
}



program ::= program_parts. {
	if(assembler_pass == 1) {
		/* prepping for pass 2... zero the blocks */
		memset(blocks, 0, sizeof(block) * MAX_CNT);
		block_cnt = 0;
	}
	else if(assembler_pass == 2) {
		m_uword i;
		int j;
		m_uword file_flag_word = M_FILE_ID;
		m_uword block_flag_word = M_BLOCK_SEPERATOR;
		m_uword block_type = 1; /* 1 is flash */
		m_uword zero = 0;
		FILE *outfile = fopen(filename, "wb");
		if(outfile == NULL) {
			fprintf (stderr, "Error - could not open output file for writing.\n");
			exit(1);
		}

		printf("0x%08X\n%d\n================\n", file_flag_word, block_cnt);
		fwrite(&file_flag_word, sizeof(m_uword), 1, outfile);
		fwrite(&block_cnt, sizeof(m_uword), 1, outfile);

		for(i = 0; i < block_cnt; i++) {
			block *b = &(blocks[i]);
			block_item *item = b->head;
			printf("0x%08X\n", block_flag_word);
			printf("0x%08X\n", block_flag_word);
			fwrite(&block_flag_word, sizeof(m_uword), 1, outfile);
			fwrite(&block_flag_word, sizeof(m_uword), 1, outfile);

			printf("%d\n", i);
			fwrite(&i, sizeof(m_uword), 1, outfile);


			printf("%d\n", block_type);
			fwrite(&block_type, sizeof(m_uword), 1, outfile);

			if(b->node_cnt > 0) {
				m_uword length = b->tail->address - item->address + (b->tail->is_instruction ? 2 : 1);
				printf("0x%08X\n", item->address);
				fwrite(&(item->address), sizeof(m_uword), 1, outfile);

				printf("0x%08X\n", length);
				fwrite(&length, sizeof(m_uword), 1, outfile);

				printf("0x%08X\n", block_flag_word);
				fwrite(&block_flag_word, sizeof(m_uword), 1, outfile);

				printf("---------------------\n");
			}
			else {
				printf("0x00000000\n0x00000000\n");
				fwrite(&zero, sizeof(m_uword), 1, outfile);
				fwrite(&zero, sizeof(m_uword), 1, outfile);

				printf("0x%08X\n", block_flag_word);
				fwrite(&block_flag_word, sizeof(m_uword), 1, outfile);
			}
			
			for(j = 0; j < b->node_cnt; j++) {
				printf("0x%08X\t:0x%04X\n", item->words[0], item->address);
				fwrite(&(item->words[0]), sizeof(m_uword), 1, outfile);

				if(item->is_instruction) {
					printf("0x%08X\t:0x%04X\n", item->words[1], item->address + 1);
					fwrite(&(item->words[1]), sizeof(m_uword), 1, outfile);
				}
				item = item->next;
			}

			printf("================\n");
		}

	}
}


program_parts ::= eols preamble address_blocks.
program_parts ::= preamble address_blocks.

eols ::= eols EOL.
eols ::= EOL.

preamble ::= uses_lines mode_lines constant_lines variable_lines.

uses_lines ::= uses_lines uses_line eols.
uses_lines ::= uses_lines uses_line error eols.
uses_lines ::= .

uses_line ::= USES_DIRECTIVE(dir) STRING. {
	if(assembler_pass == 1) {
		warning(dir, dir.token_str, "uses directive is currently unsupported");
	}
}

mode_lines ::= mode_lines mode_line eols.
mode_lines ::= mode_lines mode_line error eols.
mode_lines ::= .

mode_line ::= MODE_DIRECTIVE NOP_DELAY. {
	if(assembler_pass == 1) {
		mode_nop_delay = 1;
	}
}
mode_line ::= MODE_DIRECTIVE NO_NOP_DELAY. {
	if(assembler_pass == 1) {
		mode_nop_delay = 0;
	}
}
mode_line ::= MODE_DIRECTIVE SIMPLE_VARS. {
	if(assembler_pass == 1) {
		mode_simple_vars = 1;
	}
}
mode_line ::= MODE_DIRECTIVE NO_SIMPLE_VARS(nsv). {
	if(assembler_pass == 1) {
		warning(nsv, nsv.token_str, "this mode directive is currently unsupported");
	}
}

constant_lines ::= constant_lines constant_line eols.
constant_lines ::= constant_lines constant_line error eols.
constant_lines ::= .

variable_lines ::= variable_lines variable_line eols.
variable_lines ::= variable_lines variable_line error eols.
variable_lines ::= .

constant_line ::= CONST_DIRECTIVE IDENTIFIER(id) constant_immediate(immed). {
	if(assembler_pass == 1) {
		int pos = constant_pos(id.token_str);
		if(pos >= 0) {
			char s[100];
			sprintf(s, "Constant already defined as 0x%X.", constant_values[pos]);
			error(id, s, NULL);
		}
		else if(constant_cnt >= MAX_CNT) {
			error(id, "Too many constants defined.", id.token_str);
		}
		else if((pos = variable_pos(id.token_str)) >= 0 ) {
			char s[100];
			sprintf(s, "Constant name in use by variable at address 0x%X.", variable_addrs[pos]);
			error(id, s, NULL);		
		}
		else {
			/* new const defined */
			int len = strlen(id.token_str) + 1;
			constants[constant_cnt] = malloc(sizeof(char) * len);
			strcpy(constants[constant_cnt], id.token_str);
			constant_values[constant_cnt] = immed.data;
			constant_cnt++;
		}
	}
}


constant_immediate(val) ::= number(num). {
	memcpy(&val, &num, sizeof(Token));
}
constant_immediate(val) ::= CHARACTER(ch). {
	val.data = ch.data;
}


variable_line ::= VAR_DIRECTIVE IDENTIFIER(id). {
	if(assembler_pass == 1) {
		int pos = variable_pos(id.token_str);
		if(pos >= 0) {
			char s[100];
			sprintf(s, "Variable name in use by variable at address 0x%X.", variable_addrs[pos]);
			error(id, s, NULL);
		}
		else if(variable_cnt >= MAX_CNT) {
			error(id, "Too many variables defined.", id.token_str);
		}
		else if((pos = constant_pos(id.token_str)) >= 0 ) {
			char s[100];
			sprintf(s, "Variable already a constant defined as 0x%X.", constant_values[pos]);
			error(id, s, NULL);		
		}
		else	if(variable_top - 1 < 0) {
			error(id, "out of space for variable", NULL);
		}
		else {
			/* new variable defined */
			int len = strlen(id.token_str) + 1;
			variables[variable_cnt] = malloc(sizeof(char) * len);
			strcpy(variables[variable_cnt], id.token_str);

			variable_top--;
			variable_addrs[variable_cnt] = variable_top;

			variable_cnt++;			
		}
	}	
}
variable_line ::= VAR_DIRECTIVE IDENTIFIER(id) LBRACKET number(num) RBRACKET. {
	if(assembler_pass == 1) {
		int pos = variable_pos(id.token_str);
		if(pos >= 0) {
			char s[100];
			sprintf(s, "Variable name in use by variable at address 0x%X.", variable_addrs[pos]);
			error(id, s, NULL);
		}
		else if(variable_cnt >= MAX_CNT) {
			error(id, "Too many variables defined.", id.token_str);
		}
		else if((pos = constant_pos(id.token_str)) >= 0 ) {
			char s[100];
			sprintf(s, "Variable already a constant defined as 0x%X.", constant_values[pos]);
			error(id, s, NULL);		
		}
		else	if(num.data < 1) {
			error(id, "negative array sizes are not allowed", num.token_str);
		}
		else if((variable_top - num.data) < 0) {
			error(id, "array size consumes the rest of memory", num.token_str);
		}
		else {			
			/* new variable defined */
			int len = strlen(id.token_str) + 1;
			variables[variable_cnt] = malloc(sizeof(char) * len);
			strcpy(variables[variable_cnt], id.token_str);

			variable_top -= num.data;
			variable_addrs[variable_cnt] = variable_top;

			variable_cnt++;
		}
	}	
}
variable_line ::= VAR_DIRECTIVE IDENTIFIER(id) LBRACKET number(num_low) COMMA number(num_high) RBRACKET. {
	if(assembler_pass == 1) {
		int array_size = num_high.data - num_low.data + 1;
		int array_base = variable_top - num_high.data - 1;
		int pos = variable_pos(id.token_str);
		if(pos >= 0) {
			char s[100];
			sprintf(s, "Variable name in use by variable at address 0x%X.", variable_addrs[pos]);
			error(id, s, NULL);
		}
		else if(variable_cnt >= MAX_CNT) {
			error(id, "Too many variables defined.", id.token_str);
		}
		else if((pos = constant_pos(id.token_str)) >= 0 ) {
			char s[100];
			sprintf(s, "Variable already a constant defined as 0x%X.", constant_values[pos]);
			error(id, s, NULL);		
		}
		else	if((signed)num_low.data >= (signed)num_high.data) {
			error(id, "reverse order and zero array sizes are not allowed", NULL);
		}
		else if((variable_top - array_size) < 0) {
			error(id, "array size consumes the rest of memory", NULL);
		}
		else {		
			warning(id, "arbitrarily indexed arrays are tricky", "use at own risk");
			
			/* new variable defined */
			int len = strlen(id.token_str) + 1;
			variables[variable_cnt] = malloc(sizeof(char) * len);
			strcpy(variables[variable_cnt], id.token_str);

			variable_top -= array_size;
			variable_addrs[variable_cnt] = array_base;

			variable_cnt++;
		}
	}	
}


/* 
 * The lines have EOLs, not the blocks... 
 * that's why there's no EOL rule 
 */

address_blocks ::= address_blocks address_block.
address_blocks ::= address_blocks error.
address_blocks ::= .

address_block ::= address_line code_lines. {
	/*
	 * This only executes after the entire block has been parsed and validated
	 */
	if(assembler_pass == 2) {
		/* ??? */
		block_cnt++;		
	}
}


address_line ::= ADDR_DIRECTIVE number(num) eols. {
	/*
	 * We're beginning a new block whose block number is
	 * the current block_cnt.  If the previous block did not
	 * fully parse, this will replace it in the blocks array.
	 */
	if(assembler_pass == 2) {
		block *b = &(blocks[block_cnt]);
		block_item *item;
		/* wipe out any existing items if previous block failed */
		while((item = b->head)) {
			b->head = item->next;
			free(item);
		}
		/* head should be NULL, fix tail */
		b->tail = NULL;
		b->node_cnt = 0;
	}
	address = num.data;

}
address_line ::= ADDR_DIRECTIVE number error eols.

code_lines ::= code_lines label_line eols.
code_lines ::= code_lines instruction_line eols.
code_lines ::= code_lines raw_line eols.
code_lines ::= code_lines label_line error eols.
code_lines ::= code_lines instruction_line error eols.
code_lines ::= code_lines raw_line error eols.
code_lines ::= .

label_line(val) ::= LABEL_DIRECTIVE IDENTIFIER(id). {
	memcpy(&val, &id, sizeof(Token));
	if(assembler_pass == 1) {
		int pos = label_pos(id.token_str);
		if(pos >= 0) {
			char s[100];
			sprintf(s, "Label already defined at address 0x%X.", label_addrs[pos]);
			error(id, s, id.token_str);
		}
		else if(label_cnt >= MAX_CNT) {
			error(id, "Too many labels defined.", id.token_str);
		}
		else {
			/* new label defined */
			int len = strlen(id.token_str) + 1;
			labels[label_cnt] = malloc(sizeof (char) * len);
			strcpy(labels [label_cnt], id.token_str);
			label_addrs[label_cnt] = address;
			
			label_cnt++;
		}
	}
}

instruction_line ::= math_logic_line.
instruction_line ::= data_line.
instruction_line ::= branch_line. {
	nop_delay_slots();
}
instruction_line ::= int_line. {
	nop_delay_slots();
}
instruction_line ::= iret_line. {
	nop_delay_slots();
}

instruction_line ::= neg_line.
instruction_line ::= inv_line.
instruction_line ::= movr_line.
instruction_line ::= movi_line.
instruction_line ::= bra_line. {
	nop_delay_slots();
}
instruction_line ::= nop_line.

/* 
 * filling the delay slot on this one is not necessary, but since it is implemented as a branch
 * we'll fill it anyway.
 */
instruction_line ::= flush_line. {
	nop_delay_slots();
}




math_logic_line ::= predicate(p) math_logic_instruction(inst) REGISTER(reg_a) comma REGISTER(reg_b) comma paren_expression(exp). {
	insert_instruction(p.data, inst.data, 0, reg_a.data, reg_b.data, exp.reg_c, exp.immed);
}
data_line ::= predicate(p) data_instruction(inst) REGISTER(reg_a) comma bracket_expression(exp). {
	insert_instruction(p.data, inst.data, 0, reg_a.data, 0, exp.reg_c, exp.immed);
}
branch_line ::= predicate(p) branch_instruction(inst) hint(h) comma REGISTER(reg_a) comma REGISTER(reg_b) comma bracket_expression(exp). {
	insert_instruction(p.data, inst.data, h.data, reg_a.data, reg_b.data, exp.reg_c, exp.immed);
}
int_line ::= predicate(p) INT paren_expression(exp). {
	insert_instruction(p.data, M_INT, 0, 0, 0, exp.reg_c, exp.immed);
}
iret_line ::= predicate(p) IRET. {
	insert_instruction(p.data, M_IRET, 0, 0, 0, 0, 0);
}
neg_line ::= predicate(p) NEG REGISTER(reg_a) comma REGISTER(reg_c). {
	insert_instruction(p.data, M_SUB, 0, reg_a.data, 0, reg_c.data, 0);
}
inv_line ::= predicate(p) INV REGISTER(reg_a) comma REGISTER(reg_b). {
	insert_instruction(p.data, M_EXOR, 0, reg_a.data, reg_b.data, 0, ~0U);
}
movr_line ::= predicate(p) MOVR REGISTER(reg_a) comma REGISTER(reg_c). {
	insert_instruction(p.data, M_ADD, 0, reg_a.data, 0, reg_c.data, 0);
}
movi_line ::= predicate(p) MOVI REGISTER(reg_a) comma immediate(immed). {
	insert_instruction(p.data, M_ADD, 0, reg_a.data, 0, 0, immed.data);
}
bra_line ::= predicate(p) BRA bracket_expression(exp). {
	insert_instruction(p.data, M_BRAE, 1, 0, 0, exp.reg_c, exp.immed);
}
nop_line ::= predicate(p) NOP. {
	insert_instruction(p.data, M_ADD, 0, 0, 0, 0, 0);
}
flush_line ::= predicate(p) FLUSH. { 
	insert_instruction(p.data, M_BRAE, 0, 0, 0, M_REGISTER_PC, 0);
}


predicate(val) ::= PREDICATE(p). {
	memcpy(&val, &p, sizeof(Token));
	val.data = 1; /* predicate based on r1 */
}	
predicate(val) ::= . {
	memset(&val, 0, sizeof(Token));
	val.data = 0; /* predicate based on r0, i.e., just run it :-) */
}	


hint(val) ::= HINT(h). {
	if(h.token_str[1] == 'T' || h.token_str[1] == 't') {
		val.data = 1;
	}
	else {
		val.data = 0;
	}
}
hint(val) ::= . {
	val.data = 0; /* default false prediction */
}


raw_line ::= number(num). {
	insert_raw(num.data);
}

raw_line ::= CHARACTER(c). {
	insert_raw(c.data);
}

raw_line ::= STRING(s). {
	int i;
	for(i = 0; i < strlen(s.token_str) + 1; i++) {
		insert_raw(s.token_str[i]);
	}
}

raw_line ::= PACKED_STRING(ps). {
	m_uword word;
	char * s = ps.token_str;
	int len = strlen(s) + 1;

	/*
	 * first do whole words 
	 */
	while(len >= 4) {
		int i;
		word = 0;
		for(i = 3; i >=0; i--) {
			word <<= 8;
			word |= s[i] & ~(~0 << 8);			
		}
		insert_raw(word);
		len -= 4;
		s += 4;
	}	

	/*
	 * finish with partial word
	 */
	if(len > 0) {
		int i;
		word = 0;
		for(i = len - 1; i >=0; i--) {
			word <<= 8;
			word |= s[i] & ~(~0 << 8);			
		}
		insert_raw(word);	
	}

}

math_logic_instruction(val) ::= ADD. { 
	val.data = M_ADD; 
} 
math_logic_instruction(val) ::= SUB. { 
	val.data = M_SUB; 
} 
math_logic_instruction(val) ::= MULT. { 
	val.data = M_MULT; 
}
math_logic_instruction(val) ::= DIV. { 
	val.data = M_DIV; 
}
math_logic_instruction(val) ::= MOD. { 
	val.data = M_MOD; 
} 

math_logic_instruction(val) ::= AND. { 
	val.data = M_AND; 
} 
math_logic_instruction(val) ::= OR. { 
	val.data = M_OR; 
}
math_logic_instruction(val) ::= EXOR. { 
	val.data = M_EXOR; 
} 
math_logic_instruction(val) ::= SHL. { 
	val.data = M_SHL; 
}
math_logic_instruction(val) ::= SHR. { 
	val.data = M_SHR; 
} 

data_instruction(val) ::= LOAD. { 
	val.data = M_LOAD; 
}
data_instruction(val) ::= STOR. { 
	val.data = M_STORE; 
}
data_instruction(val) ::= RLOAD. { 
	val.data = M_RLOAD; 
} 
data_instruction(val) ::= RSTOR. { 
	val.data = M_RSTORE; 
}

branch_instruction(val) ::= BRANE. { 
	val.data = M_BRANE; 
}
branch_instruction(val) ::= BRAE. { 
	val.data = M_BRAE; 
}
branch_instruction(val) ::= BRAL. { 
	val.data = M_BRAL; 
}
branch_instruction(val) ::= BRALE. { 
	val.data = M_BRALE; 
} 
branch_instruction(val) ::= BRAG. { 
	val.data = M_BRAG; 
}
branch_instruction(val) ::= BRAGE. { 
	val.data = M_BRAGE; 
}

comma ::= COMMA.
comma ::= .

paren_expression(val) ::= LPAREN rc_plus_immed(exp) RPAREN. {
	memcpy(&val, &exp, sizeof(Token));
}
bracket_expression(val) ::= LBRACKET rc_plus_immed(exp) RBRACKET. {
	memcpy(&val, &exp, sizeof(Token));
}


rc_plus_immed(val) ::= REGISTER(reg) ARITHMETIC_OPERATOR(op) immediate(x). {
	memcpy(&val, &reg, sizeof(Token));
	val.is_rci_exp = 1;
	val.reg_c = reg.data;
	val.immed = (m_uword)((m_sword)x.data * (op.token_str[0] == '+' ? 1 : -1));
}
rc_plus_immed(val) ::= REGISTER(reg). {
	memcpy(&val, &reg, sizeof(Token));
	val.is_rci_exp = 1;
	val.reg_c = reg.data;
	val.immed = 0;
}
rc_plus_immed(val) ::= immediate(x). {
	memcpy(&val, &x, sizeof(Token));
	val.is_rci_exp = 1;
	val.reg_c = 0;
	val.immed = x.data;
}


immediate(val) ::= number(num). {
	memcpy(&val, &num, sizeof(Token));
}
immediate(val) ::= CHARACTER(ch). {
	memcpy(&val, &ch, sizeof(Token));
}
immediate(val) ::= IDENTIFIER(id). {
	if(assembler_pass == 2) {
		int pos = variable_pos(id.token_str);
		if(pos >= 0) {
			val.data = variable_addrs[pos];		
		}
		else {
			pos = constant_pos(id.token_str);
			if(pos >= 0) {
				val.data = constant_values[pos];		
			}
		}
				
		if(pos < 0) {
			error(id, "No such variable/constant defined.", id.token_str);
			val.data = 0;
		}
	}
}
immediate(val) ::= label_immediate(immed). {
	memcpy(&val, &immed, sizeof(Token));
}

label_immediate(val) ::= LABEL_DIRECTIVE IDENTIFIER(id). {
	memcpy(&val, &id, sizeof(Token));
	if(assembler_pass == 2) {
		int pos = label_pos(id.token_str);
		if(pos < 0) {
			error(id, "No such label defined.", id.token_str);
			val.data = 0;
		}
		else {
			val.data = label_addrs[pos];
		}
	}
}

number(val) ::= NUMBER(num). {
	memcpy(&val, &num, sizeof(Token));
}
number(val) ::= ARITHMETIC_OPERATOR(op) NUMBER(num). {
	memcpy(&val, &num, sizeof(Token));
	val.data = val.data * (op.token_str[0] == '+' ? 1 : -1);
}

