<<<
 	This is an example assembly file for the new and improved 
 	MiniAT assembler, "mash".  Triple side-carrots are used to
 	form block comments. 
>>> 



<<< 
	".uses" directives must be essentially first in any source file.
	They can only follow other ".uses" directives, comments, and
	whitespace.  This is a sort of source-embedded linking structure,
	NOT a textual inclusion of other source files as in the C/C++
	"#include" directive or the incorporation of ".inc" files as done
	in other assemblers. 
>>>
.uses "helper_functions.asm"	
.uses "miniat_constants.asm"



<<<
	".mode" dircetives immediately follow the ".uses" directives
	and apply only to the source file they appear in.
>>>
.mode NOP_DELAY   # by default, delay slots are primed with NOPs (otherwise, mode NO_NOP_DELAY)
.mode SIMPLE_VARS # by default, variable addresses are chosen by mash (otherwise, NO_SIMPLE_VARS)



<<<	
	".const" (or ".constant"s) cannot have the same identifier as any variable.
>>>
.constant BIN_EXAMPLE 0b_1101_0011_0000_1110   # you could use ".const", as well
.constant HEX_EXAMPLE 0x_FACE                  # 0xFACE or 0x_F_ACE t0xFACE or 0x_F_ACE too
.constant DEC_EXAMPLE -1024                    # -1_024 if you like (sign only for decimal)
.constant OCT_EXAMPLE 0c_437



<<< 
	variables declared with ".variable" or ".var" are automatically
	placed starting at the end of RAM (0x19FF) in the default mode,
	SIMPLE_VARS.  Variables may not have the same identifier as any
	constant.
>>>
.variable my_var # 0x19FF
.variable zero_array[10]     # 0x19F5 through 0x19FE, zero_array[0] is 0x19F5
.variable offset_array[-2,2] # 0x19F0 through 0x19F4, offset_array[0] is 0x19F2



##################################
# ...so ends the preamble
# ...thus beginith the meat



<<<
	".address" directives tell mash the follwing code, labels, and 
	variable declarations begin at the given address.  Every program
	must have at least one of these if any code is given, otherwise
	mash will not know where the code should be loaded into RAM.
>>>
.address 0x2000

	### ARITHMETIC INSTRUCTIONS ###
	ADD 	r6, 	r4,	(r5 + 104)
	SUB 	r3,	r8,	(r0 + 'A')
	MULT	r13,	r7,	(r2 + 30)
	DIV	r8,	r5,	(r7 + 14)
	MOD	r10,	r20,	(r30 + 17)

	### LOGIC INSTRUCTIONS ###
	AND	r2,	r3,	(r4 + 0x98765432)
	OR	r5,	r6,	(r7 + 0xBADCC0DE)
	EXOR	r8,	r9,	(r10 + 0xFEEDCAFE)
	SHL	r11,	r12,	(r13 + 0xBA5EBA11)
	SHR	r14,	r15,	(r1 + 0xB01DFACE)

	### DATA INSTRUCTIONS ###
	LOAD	r2,	[r3 + 4]
	STOR	r5,	[r4 - 1]
	RLOAD	r2,	[r3 + 4]
	RSTOR	r5, 	[r4 - 1]

<<<
	The following are special register mneumonics:
	"RSP" == r253
	"SP"  == r254
	"PC"  == r255
>>>

.address 0x2250
<<<
	Labels are equivalent to the immediate address of whatever the next 
	line of code would be, if one existed in the same address block.
>>>
!label1 # 0x2250
	
	### CONTROL INSTRUCTIONS ###
	BRAE	r3,	r4,	[r2 + 0x1ADA]
	BRANE	r3,	r4,	[r2 + 0x1ADA]
	BRAL	r12,	r13,	[r11 + 0x2000]
	BRALE	r15,	r1,	[r14 + 0x1BAA]
	BRAG	r12,	r13,	[r11 + 0x2000]
	BRAGE	r15,	r1,	[r14 + 0x1BAA]
<<<
	Each of the previous lines are 2 words w/ 6 words of NOP padding because of the NOP_DELAY
	mode set in the preamble above.
	
	If NO_NOP_DELAY mode is set, each of the previous lines are only 2 words
>>>
!label2 # 0x2280 in NOP_DELAY, 0x225C in NO_NOP_DELAY
	INT	(r9 + 1)
	IRET
	BRAE	r0,	r0,	[!label1]
	BRAE	r0,	r0,	[r5]
	BRAE	r0,	r0,	[r3 + !label2]

	# The follwing two examples hint the branch predictor as to the expected conditon 
	BRAE	{T}	r2,	r5,	[!label1]
	BRAE	{F}	r2,	r5,	[!label2]


	### PSEUDOINSTRUCTIONS ###
	NEG	r4,	r5
	INV	r7,	r11
	MOVR	r5,	r6
	MOVI	r3,	17
	MOVR	r8,	r20
	BRA	[r2 + 0x2300]
	NOP
	FLUSH


<<<
	As a general note about instruction formatting, since the most
	distinguishing feature of the instruction set is the adddition
	of the third encoded register and a full word immediate, we've 
	chosen to emphasize this in the mash grammar.  This is seen in
	nearly all the examples above in either parenthetical or braced
	expressions.  In the interest of brevity, the mash syntax affords
	several alternate forms of these expressions.    

	For parenthetical expressions, used only in arithmetic and logic
	instructions, only the following forms are permitted:
	
	(r4 + 19)
	(r4)       # an immediate of 0 is implied
	(19)       # r0 is implied
	
	Similarly, for braced expressions, only the following forms are 
	permitted:

	[r4 + 19]
	[r4]       # an immediate of 0 is implied
	[19]       # r0 is implied
>>>

<<<
	Predicating an instruction is done by leading the line with two
	underscores.  This visually calls attention to the line(s) quite
	harshly to ensure they are impossible to mistake for non-
	predicated instructions.  This style also gives the visual
	sensation of code tabbed within a conditional scope in some high	
	level language to reinforce (through cognitive reflex) the line
	executes conditionally.
>>>
	SUB	r1,	r15,	(r1 + 3)
	__ADD	r1,	r0,	(1)  # ensure r1 is never 0
	DIV	r4,	r5,	(r1)


<<< 
	Raw values allow the writer to drop raw decimal, hex, octal, or binary words into
	an address block along with labels to locate the values.  These could effectively be
	code or (un)initialized data.
	
	Raw values also support single character data enclosed in 's.  Currently no support exists
	for the inclusion of the sigle quote (or other special characters) as the character data.
	
	Quoted strings may also be used and become NULL terminated.
	
	Packed strings are enclosed in 's.  Mash stores each consecutive 4 character sequence
	in a word with the earliest character from the 4 stored in the lowest order byte of
	the word.  Packed strings are NULL terminated w/ the NULL packed as part of the string,
	as well.
>>>

.address 0x2500

!some_characters
	'A'
	'a'
	'@'
	
!string1
	"abcdefg"
	
!string2
	"HIJKLMNOP"	
	
!packed_string
	'MiniAT is cool' 

!other_raw_stuff
	0b_00000000000000000000000000000001
	0b_00000000000000000000000000000010
	0b_00000000000000000000000000000100
	0b_00000000000000000000000000001000

	0b_00000000000000000000000000010000
	0b_00000000000000000000000000100000
	0b_00000000000000000000000001000000
	0b_00000000000000000000000010000000

	0b_00000000000000000000000100000000
	0b_00000000000000000000001000000000
	0b_00000000000000000000010000000000
	0b_00000000000000000000100000000000

	0b_00000000000000000001000000000000
	0b_00000000000000000010000000000000
	0b_00000000000000000100000000000000
	0b_00000000000000001000000000000000

	0b_00000000000000010000000000000000
	0b_00000000000000100000000000000000
	0b_00000000000001000000000000000000
	0b_00000000000010000000000000000000

	0b_00000000000100000000000000000000
	0b_00000000001000000000000000000000
	0b_00000000010000000000000000000000
	0b_00000000100000000000000000000000

	0b_00000001000000000000000000000000
	0b_00000010000000000000000000000000
	0b_00000100000000000000000000000000
	0b_00001000000000000000000000000000

	0b_00010000000000000000000000000000
	0b_00100000000000000000000000000000
	0b_01000000000000000000000000000000
	0b_10000000000000000000000000000000


