/*
 * Name        : token.h
 * Author      : William "Amos" Confer
 * 
 * License     : Copyright 2008, 2009, 2012 William "Amos" Confer
 *
 *               This file is part of MiniAT.
 *
 *               MiniAT is free software: you can redistribute it and/or modify
 *               it under the terms of the GNU General Public License as published by
 *               the Free Software Foundation, either version 3 of the License, or
 *               (at your option) any later version.
 *
 *               MiniAT is distributed in the hope that it will be useful,
 *               but WITHOUT ANY WARRANTY; without even the implied warranty of
 *               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *               GNU General Public License for more details.
 *
 *               You should have received a copy of the GNU General Public License
 *               along with MiniAT.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TOKEN_H_
#define _TOKEN_H_

#include "miniat/miniat.h"


typedef struct Token Token;

struct Token {
	char *token_str;
	int line_num;
	int char_pos;

	m_uword data;	/* for immediates and GPR#*/
	
	int is_rci_exp;  /* boolean to indicate whether it's a rC+I expression */
	m_uword reg_c;
	m_uword immed;
};

#endif /* _TOKEN_H_ */

