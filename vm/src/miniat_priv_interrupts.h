#ifndef _MINIAT_PRIV_INTERRUUPTS_H_
#define _MINIAT_PRIV_INTERRUUPTS_H_

#include "miniat/miniat.h"

/**
 * Loads the miniat's IVBT with the interrupts vector address located in the IVT for all interrupts
 * in trans_bits.
 * @param m
 * 	The miniat whose IVBT needs to be loades
 * @param trans_bitsionts_l
 *  The low transition bits
 * @param trans_bits_h
 *  The high transition bits
 */
extern void m_interrupts_load_ivbt(miniat *m, m_uword trans_bits_l, m_uword trans_bits_h);

/**
 * Find the highest priority interrupt in the low and high registers.
 * @note
 *  Big O Log(n) -- Backed by binary bitwise search
 *
 * @param in_register_l
 *  The low register to check (IQR_L)
 * @param in_register_h
 *  The high register to check (IQR_H)
 * @return
 *  The highest priority interrupt
 */
extern int m_interrupts_priority_scan(m_uword in_register_l, m_uword in_register_h);

/**
 * Cause the miniat to handle interrupts, should be called on every clock cycle.
 * @param m
 *  The miniat whose interrupts to handle
 */
extern void m_interrupts_handle_interrupt(miniat *m);

/**
 * Initialize the miniat interrupt subsytem
 * @param m
 *  The miniat whose interrupt subsytem needs initialization
 */
extern void m_interrupts_new(miniat *m);

#endif
