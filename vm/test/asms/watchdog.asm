.mode NO_NOP_DELAY

.const WD_COMPARE			0x00001B1F
.const WD_CONTROL			0x00001B20
.const IFR_L 				0x00001F1C
.const IER_L				0x00001F1A
.const SYS_REG 				0x00001B1E
.const IVT_WD				0x00001ADF



.address 0x2000

!main

# Initialize the Vector Table
	MOVI r2, !watchdog_interrupt

	STOR r2, [IVT_WD]

<<<
	You could speed this up by doing all the adds ahead of the stores....
	Set up the watchdog CPS (2) and compare of (10) so 2^2 = 4 and 10 prescaled ticks,
	so we should get int every 40 cycles
>>>
	MOVI r3, 0x_1000_000A
	STOR r3, [WD_COMPARE]

# Turn on IER_L
	MOVI r4, 0x_FFFF_FFFF
	STOR r4, [IER_L]

# Turn on interrupts globably
	MOVI r5, 1
	STOR r5, [SYS_REG]

# Set up the watchdog Timer Control by enableing the watchdog timer
	MOVI r6, 0x_8000_0000
	STOR r6, [WD_CONTROL]

# Jump to a NOP region of memory and ensure nothing is in the delay slot
	BRA [0x_0000_3000]
	NOP



!watchdog_interrupt

# Interrupt routine
	ADD r1, r1, (10)
# disable watchdog timer
	STOR r0, [WD_CONTROL]
# Enable watchdog timer
	MOVI r3, 0x_8000_0000
	STOR r3, [WD_CONTROL]
	IRET
